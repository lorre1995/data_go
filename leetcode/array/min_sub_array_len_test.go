package array

import (
	"fmt"
	"testing"
)

// 长度最小的子数组
func TestMinSubArrayLen(t *testing.T) {
	fmt.Println(minSubArrayLen1(7, []int{2, 3, 1, 2, 4, 3}))
	fmt.Println(minSubArrayLen2(7, []int{2, 3, 1, 2, 4, 3}))
}

// 方法一：暴力法
// 时间复杂度：O(n*n)
// 空间负载度：O(1)
func minSubArrayLen1(target int, nums []int) int {
	var result int
	numsLen := len(nums)
	// 特殊判断
	if numsLen == 0 {
		return result
	}

	for i, numI := range nums {
		// 统计以i开头的和
		sum := numI
		// 满足条件时，1是长度最小的情况，直接退出
		if sum >= target {
			result = 1
			break
		}
		for j := i + 1; j < numsLen; j++ {
			sum += nums[j]
			// 满足条件
			if sum >= target {
				// 计算当前满足条件的子数组长度
				temp := j - i + 1
				// 是第一个满足条件的值或者比已有结果更短时，纪录最优的结果
				if result == 0 || temp < result {
					result = temp
					break
				}
			}
		}
	}
	return result
}

// 方法二：滑动窗口
// 时间复杂度：O(n)
// 空间负载度：O(1)
func minSubArrayLen2(target int, nums []int) int {
	var result int
	numsLen := len(nums)
	// 特殊判断
	if numsLen == 0 {
		return result
	}

	// 纪录滑动窗口内的值
	var sum int
	// 左边界
	var l int
	for r, num := range nums {
		// 右边界++，滑动窗口内的值+sum
		sum += num
		// 满足条件时，循环判断
		for sum >= target {
			// 计算当前滑动窗口的长度
			temp := r - l + 1
			// 第一个满足条件的滑动窗口或比已有结果更短时，纪录最优的结果
			if result == 0 || temp < result {
				result = temp
			}

			// 滑动窗口内的值-num[l]
			sum -= nums[l]
			// 左边界++
			l++
		}
	}
	return result
}
